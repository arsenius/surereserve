# SureReserve Frontend Coding #
Implementation of front end according to given screen as close as possible with limited resources I have.

### Features ###
* 100% Kotlin (multiplatform language)
* Using Google Places API as source of mock data
* Unidirectional data flow
* Modern reactive MVI approach
* Reusable, Maintainable, Scalable, Tastable code
* Android Jetpack utilization
* Best practice SOLID, Dependency Injection, ViewModel
* Single Activity App architecture
* Using device location together with bearing
* Proper device permissions handaling
* Utilize Google Play Services for better location discovery

### Drawbacks ###
* No unit tests