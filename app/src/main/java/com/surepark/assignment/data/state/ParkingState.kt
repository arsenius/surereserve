package com.surepark.assignment.data.state

import android.location.Location
import com.airbnb.mvrx.Async
import com.airbnb.mvrx.MvRxState
import com.airbnb.mvrx.Uninitialized
import com.surepark.assignment.data.model.ParkingPlace

data class ParkingState(
    val location: Async<Location> = Uninitialized,
    val currentParking: Async<List<ParkingPlace>> = Uninitialized,
    val selectedParkingLocation: Async<SingleEvent<Location>> = Uninitialized,
    val followCurrentLocation: Boolean = true,
    val totalAvailableSpots: Int = 0
): MvRxState