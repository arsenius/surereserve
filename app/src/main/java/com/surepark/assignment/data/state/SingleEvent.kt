package com.surepark.assignment.data.state

data class SingleEvent<T>(private var initValue: T?, private val defaultValue: T? = null) {

    val value: T?
        get() {
            val v = initValue
            initValue = defaultValue
            return v
        }

    val hasValue: Boolean
        get() = initValue != defaultValue

}