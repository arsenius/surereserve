package com.surepark.assignment.data.model

data class ParkingPlace (
    val id: String,
    val name : String,
    val availableSpots: Int,
    val distanceInMeters: Int,
    val travelTimeInMinutes: Int
) {
    override fun hashCode(): Int {
        return super.hashCode()
    }
}