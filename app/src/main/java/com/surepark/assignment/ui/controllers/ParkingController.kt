package com.surepark.assignment.ui.controllers

import android.view.View
import com.airbnb.epoxy.EpoxyController
import com.airbnb.mvrx.Success
import com.airbnb.mvrx.withState
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.surepark.assignment.rowParkingPlace
import com.surepark.assignment.view_model.ParkingViewModel

class  ParkingController<T: View>(private val viewModel: ParkingViewModel,
                                  private val sheetBehavior: BottomSheetBehavior<T>) : EpoxyController() {

    override fun buildModels() = withState(viewModel) {
        if(it.currentParking !is Success)
            return@withState
        val allParking = it.currentParking() ?: return@withState
        for(parking in allParking){
            rowParkingPlace {
                id("_" +parking.id)
                model(parking)
                onItemClick(View.OnClickListener {
                    viewModel.selectParkingPlace(parking)
                    sheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                })
            }
        }
    }
}