package com.surepark.assignment.ui.fragments

import android.location.Location
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.airbnb.mvrx.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.surepark.assignment.R
import com.surepark.assignment.extensions.getBitmapDescriptor
import com.surepark.assignment.extensions.setSmoothRotation
import com.surepark.assignment.extensions.showError
import com.surepark.assignment.view_model.ParkingViewModel



class ParkingMapFragment : BaseMapFragment(), OnMapReadyCallback {

    private val viewModel: ParkingViewModel by activityViewModel()
    private var cameraMoved = false
    private lateinit var googleMap: GoogleMap

    private val carIcon by lazy {
        resources.getBitmapDescriptor(
            R.drawable.ic_car, ContextCompat.getColor(
                requireContext(),
                R.color.colorAccent
            )
        )
    }

    private val parkingIcon by lazy {
        resources.getBitmapDescriptor(
            R.drawable.ic_parking_marker, ContextCompat.getColor(
                requireContext(),
                R.color.colorAccent
            ), 0f
        )
    }

    private val currentLocation by lazy {
        googleMap.addMarker(
            MarkerOptions().icon(
                carIcon
            ).position(LatLng(0.0,0.0)).anchor(0.5f, 0.5f)
        )
    }

    private val selectedParking by lazy {
        googleMap.addMarker(
            MarkerOptions().icon(
                parkingIcon
            ).position(LatLng(0.0,0.0)).anchor(0.5f, 0.5f)
        )
    }

    override fun invalidate() = withState(viewModel) {
        val location = it.location
        when {
            location is Success -> {
                updateCurrentLocation(it.followCurrentLocation, location())
                if(it.currentParking is Uninitialized)
                    viewModel.requestParkingPlaces(location())
            }
            location is Fail -> {
                viewModel.stopUpdatingLocations()
                showError(location.error) {
                    viewModel.startUpdatingLocations()
                }
            }
            it.selectedParkingLocation is Fail -> showError(it.selectedParkingLocation.error)
        }

        if(it.selectedParkingLocation is Success && it.selectedParkingLocation()?.hasValue == true)
            showSelectedParking(it.selectedParkingLocation()?.value ?: return@withState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getMapAsync(this)
    }

    override fun onMapReady(map: GoogleMap) {
        googleMap = map
        setupMap()
    }

    override fun onStart() {
        super.onStart()
        if(::googleMap.isInitialized)
            viewModel.startUpdatingLocations()
    }

    override fun onStop() {
        super.onStop()

        viewModel.stopUpdatingLocations()
    }

    private fun setupMap() {
        googleMap.setOnMarkerClickListener {
            true
        }

        googleMap.setOnCameraMoveListener {
            withState(viewModel) {
                if(it.location !is Success)
                    return@withState
                if (!cameraMoved)
                    viewModel.onFollowMode(false)
                cameraMoved = false
            }
        }

        viewModel.startUpdatingLocations()
    }

    private fun updateCurrentLocation(isFollowing: Boolean, location: Location) {
        currentLocation.position = location.toPosition()

        if(isFollowing) {
            cameraMoved = true
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation.position, 16.5f))
        }

        currentLocation?.setSmoothRotation(location.bearing)
    }

    private fun showSelectedParking(location: Location) {
        selectedParking.position = location.toPosition()
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(selectedParking.position, 16.5f))
    }

    private fun Location.toPosition(): LatLng = LatLng(latitude, longitude)

}