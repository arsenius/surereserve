package com.surepark.assignment.ui.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import com.surepark.assignment.R
import com.surepark.assignment.Settings
import com.surepark.assignment.extensions.exit
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions

abstract class PermissionsActivity : AppCompatActivity(), EasyPermissions.PermissionCallbacks {

    abstract val permissions: Array<String>
    abstract fun onPermissionsAllowed()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        processPermissions(Activity.RESULT_OK)
    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        processPermissions(Activity.RESULT_OK)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, permissions.toList()))
            AppSettingsDialog.Builder(this).build().show()
        else
            exit()
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        processPermissions(Activity.RESULT_OK)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode != Settings.googlePlayServiceRequestCode)
            processPermissions(resultCode)
    }

    private fun processPermissions(resultCode: Int) {
        when {
            EasyPermissions.hasPermissions(this, *permissions) -> onPermissionsAllowed()
            resultCode == Activity.RESULT_CANCELED -> exit()
            else -> {
                EasyPermissions.requestPermissions(
                    this, resources.getString(R.string.permissions_rationale),
                    Settings.androidPermissionsRequestCode, *permissions
                )
            }
        }
    }

}