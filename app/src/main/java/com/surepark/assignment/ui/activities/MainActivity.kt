package com.surepark.assignment.ui.activities


import android.os.Bundle
import android.view.View

import com.surepark.assignment.R
import com.surepark.assignment.Settings

class MainActivity : PermissionsActivity() {

    override val permissions: Array<String> =
        arrayOf(Settings.locationPermission)

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onPermissionsAllowed() {
        setContentView(R.layout.activity_maps)
    }

    override fun onStart() {
        super.onStart()
        window.decorView.apply {
            systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        }
    }


}
