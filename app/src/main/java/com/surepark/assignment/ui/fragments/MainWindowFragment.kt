package com.surepark.assignment.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import com.airbnb.mvrx.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.surepark.assignment.R
import com.surepark.assignment.extensions.*
import com.surepark.assignment.ui.controllers.ParkingController
import com.surepark.assignment.view_model.ParkingViewModel
import kotlinx.android.synthetic.main.bottom_sheet_parking.*
import kotlinx.android.synthetic.main.fragment_main_window.*
import kotlinx.android.synthetic.main.tools.*


/**
 * A simple [Fragment] subclass.
 */
class MainWindowFragment : BaseMvRxFragment() {

    private val viewModel: ParkingViewModel by activityViewModel()
    private lateinit var controller: ParkingController<*>
    private var isError  = false

    override fun invalidate() = withState(viewModel) {
        if(::controller.isInitialized)
            controller.requestModelBuild()
        totalSpots.text = String.format(getString(R.string.spots_available), it.totalAvailableSpots.toString())
        if(it.currentParking is Fail && !isError) {
            isError = true
            showError(it.currentParking.error) {
                isError = false
                viewModel.requestParkingPlaces(it.location() ?: return@showError)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_main_window, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragmentAndSheet()
        currentLocationClickListener()
    }

    private fun setupFragmentAndSheet() {
        val mapFragment = getMapFragment(R.id.googleMap) ?: return

        val sheetBehavior = BottomSheetBehavior.from(bottomSheetParking)
        sheetBehavior.isFitToContents = true
        sheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        sheetBehavior.hideViewsWhenExpanded(totalSpots, currentLocation)

        setSheetAndFragmentProportion(bottomSheetParking, sheetBehavior, mapFragment, 0.45f, 0.61f)
        setupParkingSearch(sheetBehavior)
        setupController(sheetBehavior)
    }

    private fun <T: View> setupParkingSearch(bottomSheetBehavior: BottomSheetBehavior<T>) {
        searchParking.onFocusChangeListener = View.OnFocusChangeListener { _, focused ->
            if(focused)
                return@OnFocusChangeListener
            searchParking.hideKeyboard()
        }

        searchParking.applyAutocomplete(bottomSheetBehavior) {
            viewModel.queryParkingPlaces(it)
        }
    }

    private fun <T: View>setupController(bottomSheetBehavior: BottomSheetBehavior<T>) {
        controller = ParkingController(viewModel, bottomSheetBehavior)
        parking.setController(controller)
    }

    private fun currentLocationClickListener() {
        currentLocation.setOnClickListener {
            viewModel.onFollowMode(true)
        }
    }

}
