package com.surepark.assignment.data_binding

import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import coil.api.load
import com.surepark.assignment.R
import com.surepark.assignment.repositories.ParkingPlacesRepository
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.first
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.lang.Exception


@BindingAdapter("placeId")
fun AppCompatImageView.loadImage(placeId: String) {
    val repo = object: KoinComponent {
        val parkingPlacesRepository by inject<ParkingPlacesRepository>()
    }
    load(R.drawable.ic_no_image)
    var job = tag
    if(job is Job)
            job.cancel()
    job = GlobalScope.launch {
        try {
            val result = repo.parkingPlacesRepository.getParkingPlaceLocationAndImage(placeId).first()
                if(result.second != null)
                    withContext(Dispatchers.Main.immediate) {
                        load(result.second) {
                            crossfade(true)
                        }
                    }
        } catch(e: Exception) { e.printStackTrace() }
    }
    tag = job
}