package com.surepark.assignment.repositories

import android.location.Location
import androidx.lifecycle.LiveData

interface LocationRepository {
    val location: LiveData<Pair<Location?, Throwable?>>
    fun start()
    fun stop()
}