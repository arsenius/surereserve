package com.surepark.assignment.repositories

import android.content.Context
import android.location.Location
import android.os.Looper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnSuccessListener
import com.surepark.assignment.R
import com.surepark.assignment.extensions.LiveDataExtension

class GpsLocationRepository(private val context: Context) : LocationCallback(),
    LocationRepository,
    OnSuccessListener<Location>,
    LiveDataExtension
{

    private val locationRequest = LocationRequest.create()?.apply {
        interval = 10000
        fastestInterval = 5000
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    private val fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
    private val client: SettingsClient = LocationServices.getSettingsClient(context)
    override val location: LiveData<Pair<Location?, Throwable?>> = MutableLiveData()

    override fun start() {
        client.checkLocationSettings(LocationSettingsRequest.Builder().build())
            .addOnSuccessListener {
                if(it.locationSettingsStates.isGpsUsable) {
                    fusedLocationClient.lastLocation.addOnSuccessListener(this)
                    fusedLocationClient.requestLocationUpdates(locationRequest, this, Looper.getMainLooper())
                }
                else
                    location.postValue(null to Throwable(context.getString(R.string.location_disabled)))
            }
            .addOnFailureListener {
                location.postValue(null to it)
            }
    }

    override fun stop() {
        fusedLocationClient.removeLocationUpdates(this)
    }

    override fun onLocationResult(locationResult: LocationResult?) {
        super.onLocationResult(locationResult)
        location.postValue((locationResult?.lastLocation ?: return) to null)
    }

    override fun onLocationAvailability(locationAvailability: LocationAvailability?) {
        super.onLocationAvailability(locationAvailability)
        if(locationAvailability?.isLocationAvailable == false)
            location.postValue(null to Throwable(context.getString(R.string.location_disabled)))
    }

    override fun onSuccess(location: Location?) {
        this.location.postValue((location ?: return) to null)
    }

}