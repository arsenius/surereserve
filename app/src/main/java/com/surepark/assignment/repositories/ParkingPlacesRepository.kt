package com.surepark.assignment.repositories

import android.graphics.Bitmap
import android.location.Location
import com.surepark.assignment.data.model.ParkingPlace
import kotlinx.coroutines.flow.Flow

interface ParkingPlacesRepository {
    fun queryParkingPlaces(query: String): Flow<List<ParkingPlace>>
    fun retrieveNearbyParking(location: Location): Flow<List<ParkingPlace>>
    fun getParkingPlaceLocationAndImage(id: String): Flow<Pair<Location, Bitmap?>>
}