package com.surepark.assignment.repositories

import android.content.Context
import android.graphics.Bitmap
import android.location.Location
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.PhotoMetadata
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.FetchPhotoRequest
import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest
import com.google.android.libraries.places.api.net.FindCurrentPlaceRequest
import com.surepark.assignment.Settings
import com.surepark.assignment.data.model.ParkingPlace
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import java.lang.Exception
import java.util.*
import java.util.concurrent.CountDownLatch
import kotlin.random.Random

class GoogleMapsParkingPlacesRepository(context: Context) : ParkingPlacesRepository {

    private val placesApi = Places.createClient(context)
    private val random = Random(Date().time)

    override fun queryParkingPlaces(query: String): Flow<List<ParkingPlace>> = flow {
        val latch = CountDownLatch(1)
        var places: List<ParkingPlace> = emptyList()
        var error: Throwable? = null

        placesApi.findAutocompletePredictions(
            FindAutocompletePredictionsRequest
                .builder()
                .setCountry(Settings.parkingCountry)
                .setQuery(query)
                .build()
        ).addOnSuccessListener {
            places = it.autocompletePredictions.map { result ->
                ParkingPlace(
                    result.placeId,
                    result.getPrimaryText(null).toString(),
                    random.nextInt(0, 100),
                    result.distanceMeters ?: 0,
                    0
                )
            }
            latch.countDown()

        }.addOnFailureListener {
            error = it
            latch.countDown()
        }

        latch.await()
        if(error != null)
            throw Exception(error)
        emit(places)
    }

    override fun retrieveNearbyParking(location: Location): Flow<List<ParkingPlace>> = flow {

        val latch = CountDownLatch(1)
        var places: List<ParkingPlace> = emptyList()
        var error: Throwable? = null

        placesApi.findCurrentPlace(FindCurrentPlaceRequest.newInstance(listOf(
            Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG
        ))).addOnSuccessListener {
            places = it.placeLikelihoods.map { result ->
                    ParkingPlace(
                        result.place.id ?: "",
                        result.place.name ?: "",
                        random.nextInt(0, 100),
                        location.distanceTo(Location("GPS").apply {
                            latitude = result.place.latLng?.latitude ?: 0.0
                            longitude = result.place.latLng?.longitude ?: 0.0
                        }).toInt(),
                        0
                    )
                }

            latch.countDown()

        }.addOnFailureListener {
            error = it
            latch.countDown()
        }

        latch.await()
        if(error != null)
            throw Exception(error)
        emit(places)
    }

    override fun getParkingPlaceLocationAndImage(id: String): Flow<Pair<Location, Bitmap?>> = flow {
        val latch = CountDownLatch(1)
        var locationAndBitmap: Pair<Location, Bitmap?> = Location("") to null
        var error: Throwable? = null

        placesApi.fetchPlace(FetchPlaceRequest.newInstance(id, listOf(
            Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.PHOTO_METADATAS
        ))).addOnSuccessListener {

            val location = Location("GPS").apply {
                latitude = it.place.latLng?.latitude ?: 0.0
                longitude = it.place.latLng?.longitude ?: 0.0
            }

            val metadata = it.place.photoMetadatas?.firstOrNull() ?: {
                locationAndBitmap = location to null
                latch.countDown()
            }()
            if (metadata is PhotoMetadata)
                placesApi.fetchPhoto(
                    FetchPhotoRequest
                        .builder(metadata)
                        .setMaxWidth(Settings.maxImageWidth)
                        .setMaxHeight(Settings.maxImageHeight)
                        .build()
                ).addOnSuccessListener { image ->
                    locationAndBitmap = location to image.bitmap
                    latch.countDown()
                }.addOnFailureListener { err ->
                    error =  err
                    latch.countDown()
                }

        }.addOnFailureListener {
            error = it
            latch.countDown()
        }
        latch.await()
        if(error != null)
            throw Exception(error)
        emit(locationAndBitmap)
    }

}