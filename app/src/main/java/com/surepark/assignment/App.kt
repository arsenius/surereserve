package com.surepark.assignment

import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.MultiDexApplication
import com.airbnb.epoxy.EpoxyAsyncUtil
import com.airbnb.epoxy.EpoxyController
import com.google.android.libraries.places.api.Places
import com.surepark.assignment.extensions.resolveGooglePlayServices
import com.surepark.assignment.service_locator.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        Places.initialize(this, getString(R.string.google_maps_key))
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        resolveGooglePlayServices()
        setupKoin()
        setupEpoxy()
    }

    private fun setupKoin() {
        startKoin {
            androidContext(this@App)
            modules(appModule)
        }
    }

    private fun setupEpoxy() {
        val handler = EpoxyAsyncUtil.getAsyncBackgroundHandler()
        EpoxyController.defaultDiffingHandler = handler
        EpoxyController.defaultModelBuildingHandler = handler
    }

    fun resolveGooglePlayServices() =
        registerActivityLifecycleCallbacks(ApplicationActivitiesLifecycle().apply {
            setStartActivityReferenceListener {
                it.resolveGooglePlayServices(it)
            }
        })

}