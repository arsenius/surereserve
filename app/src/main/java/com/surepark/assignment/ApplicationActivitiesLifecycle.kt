package com.surepark.assignment

import android.app.Activity
import android.app.Application
import android.os.Bundle

class ApplicationActivitiesLifecycle : Application.ActivityLifecycleCallbacks {

    private lateinit var onActivityRef: (Activity) -> Unit
    private var totalActivities = 0
    val isAllActivitiesInBackground: Boolean
        get() = totalActivities == 0

    fun setStartActivityReferenceListener(onActivityRef: (Activity) -> Unit) {
        this.onActivityRef = onActivityRef
    }

    override fun onActivityPaused(activity: Activity?) {

    }

    override fun onActivityResumed(activity: Activity?) {

    }

    override fun onActivityStarted(activity: Activity?) {
        totalActivities++
    }

    override fun onActivityDestroyed(activity: Activity?) {

    }

    override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {

    }

    override fun onActivityStopped(activity: Activity?) {
        totalActivities--
    }

    override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
        if (::onActivityRef.isInitialized && activity != null)
            onActivityRef(activity)
    }
}