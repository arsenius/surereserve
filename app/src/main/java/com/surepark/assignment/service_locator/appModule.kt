package com.surepark.assignment.service_locator

import com.surepark.assignment.repositories.GoogleMapsParkingPlacesRepository
import com.surepark.assignment.repositories.GpsLocationRepository
import com.surepark.assignment.repositories.LocationRepository
import com.surepark.assignment.repositories.ParkingPlacesRepository
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val appModule = module {
    single <LocationRepository> {
        GpsLocationRepository(androidContext())
    }
    single <ParkingPlacesRepository> {
        GoogleMapsParkingPlacesRepository(androidContext())
    }
}