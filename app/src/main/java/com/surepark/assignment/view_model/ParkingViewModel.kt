package com.surepark.assignment.view_model

import android.location.Location
import com.airbnb.mvrx.*
import com.surepark.assignment.data.model.ParkingPlace
import com.surepark.assignment.data.state.ParkingState
import com.surepark.assignment.data.state.SingleEvent
import com.surepark.assignment.repositories.LocationRepository
import com.surepark.assignment.repositories.ParkingPlacesRepository
import org.koin.core.KoinComponent
import org.koin.core.inject


class ParkingViewModel(
    initialState: ParkingState,
    private val locationRepo: LocationRepository,
    private val parkingRepo: ParkingPlacesRepository
) : MvRxViewModel<ParkingState>(initialState) {

    init {
        locationRepo.location.execute {
            copy(location = it)
        }
    }

    fun onFollowMode(followMode: Boolean) = setState {
        copy(followCurrentLocation = followMode)
    }

    fun requestParkingPlaces(location: Location) {
        parkingRepo.retrieveNearbyParking(location).execute {
            copy(
                currentParking = it,
                totalAvailableSpots = if(it is Success) it().sumBy { p -> p.availableSpots }
                    else totalAvailableSpots)
        }
    }

    fun queryParkingPlaces(query: String) {
        if(query.trim().isEmpty())
            withState {
                requestParkingPlaces(it.location() ?: return@withState)
            }
        else
            parkingRepo.queryParkingPlaces(query).execute {
                copy(
                    currentParking = it,
                    totalAvailableSpots = if(it is Success) it().sumBy { p -> p.availableSpots }
                        else totalAvailableSpots
                )
            }
    }

    fun selectParkingPlace(place: ParkingPlace) {
        parkingRepo.getParkingPlaceLocationAndImage(place.id).execute {
            copy(selectedParkingLocation = when(it) {
                is Loading -> Loading()
                is Fail -> Fail(it.error)
                is Success -> Success(SingleEvent(it().first))
                else -> Uninitialized
            })
        }
    }

    fun startUpdatingLocations()  {
        setState {
            copy(location = Loading())
        }
        locationRepo.start()
    }

    fun stopUpdatingLocations()  = locationRepo.stop()

    companion object : MvRxViewModelFactory<ParkingViewModel, ParkingState>, KoinComponent {
        override fun create(
            viewModelContext: ViewModelContext,
            state: ParkingState
        ): ParkingViewModel {
            val locationRepo by inject<LocationRepository>()
            val parkingRepo by inject<ParkingPlacesRepository>()
            return ParkingViewModel(ParkingState(), locationRepo, parkingRepo)
        }
    }

}