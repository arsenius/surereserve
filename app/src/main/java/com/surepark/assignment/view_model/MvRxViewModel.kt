package com.surepark.assignment.view_model

import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.airbnb.mvrx.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect

abstract class MvRxViewModel<S : MvRxState>(initialState: S) :
    BaseMvRxViewModel<S>(initialState, debugMode = BuildConfig.DEBUG) {

    private val scope = CoroutineScope(Dispatchers.IO + SupervisorJob())

    protected fun <T> Flow<T>.execute(stateReducer: S.(Async<T>) -> S) =
        scope.launch {
            setState {
                stateReducer(this, Loading())
            }
            try {
                catch {
                    setState {
                        stateReducer(this, Fail(it))
                    }
                }
                collect {
                    setState {
                        stateReducer(
                            this,
                            Success(it)
                        )
                    }
                }
            } catch (e: Exception) {
                setState {
                    stateReducer(this, Fail(e))
                }
            }
        }


    protected fun <T> LiveData<Pair<T?, Throwable?>>.execute(stateReducer: S.(Async<T>) -> S):
            Observer<Pair<T?, Throwable?>> {
                setState {
                    stateReducer(this, Loading())
                }
                val observer = Observer<Pair<T?, Throwable?>> {
                    val error = it.second
                    val result = it.first
                    if(error != null)
                        setState {
                            stateReducer(this, Fail(error))
                        }
                    else if(result != null)
                        setState {
                            stateReducer(this, Success(result))
                        }
                }
                observeForever(observer)
                return observer
    }

    override fun onCleared() {
        super.onCleared()
        scope.cancel()
    }
}