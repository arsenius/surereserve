package com.surepark.assignment.extensions

import android.content.res.Resources
import android.graphics.*
import androidx.annotation.DrawableRes
import androidx.core.content.res.ResourcesCompat
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory


fun Resources.getBitmapDescriptor(@DrawableRes iconID: Int, glowColor: Int, scaleCoefficient: Float = 2.35f): BitmapDescriptor {
    val vectorDrawable =
        ResourcesCompat.getDrawable(this, iconID, null)
    val width = vectorDrawable!!.intrinsicWidth
    val height = vectorDrawable.intrinsicHeight
    val bitmap = Bitmap.createBitmap(
        if (scaleCoefficient == 0f) width else (width*scaleCoefficient).toInt(),
        if (scaleCoefficient == 0f) height else (height*scaleCoefficient).toInt(), Bitmap.Config.ARGB_8888
    )

    val canvas = Canvas(bitmap)

    val offsetX = if (scaleCoefficient == 0f)  0 else (canvas.width - width) / 2
    val offsetY = if (scaleCoefficient == 0f) 0 else (canvas.height - height) / 2
    vectorDrawable.setBounds(offsetX, offsetY,
        width+offsetX, height+offsetY)
    vectorDrawable.draw(canvas)
    val alpha: Bitmap = bitmap.extractAlpha()
    val blurMaskFilter = BlurMaskFilter(if (scaleCoefficient == 0f) 5f else 110f, BlurMaskFilter.Blur.OUTER)
    val paint = Paint()
    paint.maskFilter = blurMaskFilter
    paint.color = glowColor
    canvas.drawBitmap(alpha, 0f, 0f, paint)

    return BitmapDescriptorFactory.fromBitmap(bitmap)
}