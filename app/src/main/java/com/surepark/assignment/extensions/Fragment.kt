package com.surepark.assignment.extensions

import android.app.Activity
import android.content.IntentSender
import android.view.View
import android.view.ViewTreeObserver
import android.view.inputmethod.InputMethodManager
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.Snackbar
import com.surepark.assignment.R


fun Fragment.getMapFragment(@IdRes fragmentId: Int): SupportMapFragment? {
    val mapFragment = childFragmentManager.findFragmentById(fragmentId) ?: return null
    return if(mapFragment is SupportMapFragment) mapFragment else null
}

fun <T: View> Fragment.setSheetAndFragmentProportion(sheet: View, sheetBehavior: BottomSheetBehavior<T>,
                                                     fragment: Fragment,
                                                     sheetPart: Float,
                                                     fragmentPart: Float
) {
    sheet.viewTreeObserver.addOnGlobalLayoutListener (object: ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            sheet.viewTreeObserver.removeOnGlobalLayoutListener(this)
            val view = fragment.view ?: return
            var layoutParams = view.layoutParams
            sheetBehavior.peekHeight = (sheet.height * sheetPart).toInt()
            layoutParams.height = (view.height * fragmentPart).toInt()
            view.layoutParams = layoutParams
            layoutParams = sheet.layoutParams
            layoutParams.height = (sheet.height * 0.95).toInt()
            sheet.layoutParams = layoutParams
        }
    })
}

fun Fragment.showError(err: Throwable, action: (() -> Unit)? = null) {
    if(err is ResolvableApiException) {
        try {
            err.startResolutionForResult(requireActivity(), 1)
        } catch (sendEx: IntentSender.SendIntentException) {}
    }
    else {
        val snackbar = Snackbar.make(view ?: return, err.message ?: getString(R.string.unknown_error),
                if(action != null) Snackbar.LENGTH_INDEFINITE else Snackbar.LENGTH_LONG)
        if(action != null)
            snackbar.setAction(getString(R.string.retry_action)) {
                action()
            }
        snackbar.show()
    }
}


