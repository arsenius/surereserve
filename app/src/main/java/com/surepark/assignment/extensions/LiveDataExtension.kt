package com.surepark.assignment.extensions

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

interface LiveDataExtension {
    fun <T> LiveData<T>.postValue(value: T) {
        val self = this
        if(self is MutableLiveData)
            self.postSyncValue(value)
    }
}