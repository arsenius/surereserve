package com.surepark.assignment.extensions

import android.app.Activity
import com.google.android.gms.common.GoogleApiAvailability
import com.surepark.assignment.Settings

fun Activity.resolveGooglePlayServicesError(
    error: Int,
    googleApi: GoogleApiAvailability =
        GoogleApiAvailability.getInstance()
) {
    if (googleApi.isUserResolvableError(error)) {
        val errorDialog = googleApi.getErrorDialog(
            this,
            error,
            Settings.googlePlayServiceRequestCode
        )
        errorDialog.setOnDismissListener {
            GoogleApiAvailability.getInstance().makeGooglePlayServicesAvailable(this)
            exit()
        }
        errorDialog.show()
    }

}

fun Activity.exit() {
    val pid = android.os.Process.myPid()
    finishAffinity()
    android.os.Process.killProcess(pid)
}