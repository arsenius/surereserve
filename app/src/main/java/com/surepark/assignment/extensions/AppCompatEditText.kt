package com.surepark.assignment.extensions

import android.app.Activity
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.widget.addTextChangedListener
import com.google.android.material.bottomsheet.BottomSheetBehavior

fun AppCompatEditText.hideKeyboard() {
    val imm: InputMethodManager =
        context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}

fun AppCompatEditText.applyAutocomplete(bottomSheetBehavior: BottomSheetBehavior<*>, onInput: (String) -> Unit) {
    addTextChangedListener {
        if(it?.trim().isNullOrEmpty())
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        else
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED

        onInput(it.toString())
    }
}