package com.surepark.assignment.extensions

import androidx.lifecycle.MutableLiveData


fun <T> MutableLiveData<T>.postSyncValue(value: T) =
    synchronized(this) {
        postValue(value)
    }