package com.surepark.assignment.extensions

import android.animation.Animator
import android.animation.ObjectAnimator
import com.google.android.gms.maps.model.Marker

fun Marker.setSmoothRotation(angle: Float) {
    val anim = ObjectAnimator.ofFloat(this, "rotation",  rotation, angle)
    anim.duration = 500
    anim.addListener(object: Animator.AnimatorListener {
        override fun onAnimationRepeat(animator: Animator) {}
        override fun onAnimationEnd(animator: Animator) {
            rotation = angle
        }
        override fun onAnimationCancel(animator: Animator) {}
        override fun onAnimationStart(animator: Animator) {}
    })
    anim.start()
}