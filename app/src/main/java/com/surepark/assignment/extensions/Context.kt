package com.surepark.assignment.extensions

import android.app.Activity
import android.content.Context
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability

inline fun Context.resolveGooglePlayServices(
    activity: Activity? = null,
    resolver: (GoogleApiAvailability) -> Unit = {}
) {
    val googleApi = GoogleApiAvailability.getInstance()
    val result = googleApi.isGooglePlayServicesAvailable(this)
    if (result != ConnectionResult.SUCCESS) {
        activity?.resolveGooglePlayServicesError(result, googleApi)
        return
    }
    resolver(googleApi)
}