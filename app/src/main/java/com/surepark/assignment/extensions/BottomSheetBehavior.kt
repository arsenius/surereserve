package com.surepark.assignment.extensions

import android.view.View
import com.google.android.material.bottomsheet.BottomSheetBehavior

fun <T: View>BottomSheetBehavior<T>.hideViewsWhenExpanded(vararg views: View) {
    setBottomSheetCallback(object: BottomSheetBehavior.BottomSheetCallback() {
        override fun onSlide(view: View, slide: Float) {

        }

        override fun onStateChanged(view: View, state: Int) {
            if(state == BottomSheetBehavior.STATE_EXPANDED)
                views.forEach {
                    it.visibility = View.INVISIBLE
                }
            else if(state == BottomSheetBehavior.STATE_COLLAPSED)
                views.forEach {
                    it.visibility = View.VISIBLE
                }
        }

    })
}