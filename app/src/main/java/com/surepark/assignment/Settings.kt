package com.surepark.assignment

import android.Manifest.permission.ACCESS_FINE_LOCATION

object Settings {
    const val locationPermission = ACCESS_FINE_LOCATION
    const val googlePlayServiceRequestCode = 9000
    const val androidPermissionsRequestCode = 1
    const val parkingCountry = "SG"
    const val maxImageWidth = 640
    const val maxImageHeight = 480
}